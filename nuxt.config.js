import colors from 'vuetify/es5/util/colors'

export default {
  // https://nuxtjs.org/api/configuration-modern
  modern: true,
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - ' + 'Hitungan Grab',
    title: 'Hitungan Grab',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: colors.green.darken2,
    failedColor: colors.deepOrange.darken2,
    height: '3px'
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  // https://pwa.nuxtjs.org/modules/meta.html#options
  meta: {
    name: 'Hitungan Grab',
    description: 'Try the easier way to split and request payment to friends',
    theme_color: colors.green.darken2,
    ogHost: 'https://hitungan-grab.himakupu.com',
    twitterCard: 'summary_large_image',
    twitterSite: '',
    twitterCreator: ''
  },
  // https://pwa.nuxtjs.org/modules/manifest.html
  manifest: {
    name: 'Hitungan Grab',
    short_name: 'Hitungan Grab',
    start_url: '/?utm_source=homescreen',
    description: 'Try the easier way to split and request payment to friends',
    lang: 'id',
    theme_color: colors.green.darken2,
    background_color: '#fff'
  },
  // https://nuxtjs.org/api/configuration-loading-indicator/
  loadingIndicator: 'nuxt',
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
